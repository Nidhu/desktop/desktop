SRC = ./src
BUILD = ./build
TARGET = /usr/bin/start-ndh
DESKTOP = ./nidhu.desktop
XSESSIONS = /usr/share/xsessions
RC = ./ndhrc
KRCS = ./*rc

prepare: 
	mkdir -p $(BUILD)

build: prepare
	cd $(BUILD) && qmake .$(SRC) && make
	mv $(BUILD)/src $(BUILD)/start-ndh

install: 
	mkdir -p ~/.config
	sudo cp $(BUILD)/start-ndh $(TARGET)
	cp $(DESKTOP) $(XSESSIONS)
	cp $(RC) ~/.config/$(RC)
	cp $(KRCS) ~/.config/
	make clean

clean:
	cd $(BUILD) && make clean
	rm -rf $(BUILD)
