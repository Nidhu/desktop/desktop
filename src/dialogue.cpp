#include "dialogue.h"

#include <QLabel>

#include <iostream>

Dialogue::Dialogue(QWidget *parent) : QWidget(parent)
{

}

void Dialogue::setTitleText(char *text) {
  QLabel *label = new QLabel((QString) text, this);

  QFont font = label->font();
  font.setPointSize(72);
  font.setBold(true);

  label->setFont(font);
}

void Dialogue::setExplainText(char *text) {
  QLabel *label = new QLabel((QString) text, this);

  QFont font = label->font();
  font.setPointSize(30);
  font.setBold(false);

  label->setFont(font);
}

int Dialogue::tryExec(char *cmd, char *textTitle, char *textExplain) {
  int _exec = std::system(cmd);

  if (_exec != 0) {
    this->setTitleText(textTitle);
    this->setExplainText(textExplain);
    this->show();
  }

  return _exec;
}
