#ifndef DIALOGUE_H
#define DIALOGUE_H

#include <QWidget>

class Dialogue : public QWidget
{
    Q_OBJECT
public:
    explicit Dialogue(QWidget *parent = nullptr);

    void setTitleText(char *text);
    void setExplainText(char *text);

    int tryExec(char *cmd, char *textTitle, char *textExplain);

signals:

};

#endif // DIALOGUE_H
