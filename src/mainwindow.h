#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDesktopWidget>

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  char **readConfig(char *configs="~/.config/nidhu/settings.ndh");
  bool setBG();

  QDesktopWidget dw;

public slots:
	void dmenu();
};
#endif // MAINWINDOW_H
