#include "mainwindow.h"
#include "dialogue.h"

#include <QApplication>
#include <QPushButton>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  MainWindow w;
  w.show();

	// FIXME
	/*
	Dialogue dock;
  dock.tryExec("kwin_x11 &", "KWin not found", "Couldn't open kwin_x11 because it isn't installed. Please install.");
  dock.tryExec("gpanel &", "GPanel not found", "Couldn't open gpanel because it isn't installed. Please install.");
  dock.tryExec("dmenu_run &", "DMenu not found", "Couldn't open dmenu because it isn't installed. Please install.");
	*/
	/*
	std::system("kwin_x11 &");
	std::system("gpanel &");
	std::system("dmenu_run &");
	std::system("cd ~");
	*/

	// Run config file
	std::system("~/.config/ndhrc &");

  return a.exec();
}
