#include "mainwindow.h"

#include <fstream>
#include <iostream>

#include <QShortcut>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
  this->setWindowFlags(Qt::CustomizeWindowHint);
  this->setFixedSize(dw.width(), dw.height());
  this->move(0, 0);

  /*
  if (!setBG()) {
    std::cout << "Warning: Couldn't set BG\n";
  }
  */
  setBG();

	// FIXME
	//QShortcut *dmenu = new QShortcut("Meta-P");
	//QObject::connect(dmenu, SIGNAL(activated()), this, SLOT(dmenu()));
}

MainWindow::~MainWindow()
{
}

char **MainWindow::readConfig(char *configs) {
  char **ret;
  size_t size = 0;
  std::string text;

  std::ifstream mIfstream(configs);
  while (getline(mIfstream, text)) {
    ret[size] = (char *) text.c_str();
    size++;
  }

  return ret;
}

bool MainWindow::setBG() {
  /*
  char **read = readConfig();
  char loc;

  for (int i = 0; i < sizeof(read); i++) {
    std::cout << read[i] << std::endl;
    if (read[i] == "BG=") {
      loc = read[i][1];
      std::cout << loc << std::endl;
    }
  }
  */

  QPixmap original((QString) "/usr/share/nidhu/BG");

  original = original.scaled(this->size(), Qt::IgnoreAspectRatio);
  QPalette palette;
  palette.setBrush(QPalette::Background, original);
  this->setPalette(palette);


  return true;
}

void MainWindow::dmenu() {
	std::system("dmenu &");
}

